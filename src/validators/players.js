function isPlayerId(value) {
  return RegExp(/^\d+$/).test(value);
}

module.exports = {
  isPlayerId,
};

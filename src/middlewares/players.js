const axios = require('axios');
const config = require('config');
const { PlayersServiceError, PlayerIdBadFormat } = require('../errors');
const { playersValidator } = require('../validators');

/**
 * Middleware used for requesting players data for web and put players data fetched in
 * ctx.state.players
 * @param {*} ctx
 * @param {*} next
 * @throws PlayersServiceError
 */
async function getPlayersData(ctx, next) {
  const url = config.get('playersServiceUrl');
  let response;
  try {
    response = await axios.get(url);
  } catch (err) {
    throw new PlayersServiceError('Players service not reachable');
  }

  if (!response.data.players && !Array.isArray(response.data.players)) {
    throw new PlayersServiceError('Players service returns a bad format for players');
  }

  ctx.state.players = response.data.players;
  await next();
}

/**
 * Middleware used to check the playerId param, sanitize the param
 * and put it in ctx.state.sanitizedPlayerId
 * @param {string} playerId
 * @param {*} ctx
 * @param {*} next
 */
async function checkPlayerIdParam(playerId, ctx, next) {
  if (!playersValidator.isPlayerId(playerId)) {
    throw new PlayerIdBadFormat(`Player Id format is not correct. It should be an Integer. Found: ${playerId}`);
  }
  ctx.state.sanitizedPlayerId = parseInt(playerId, 10);

  return next();
}

module.exports = {
  getPlayersData,
  checkPlayerIdParam,
};

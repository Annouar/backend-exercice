/**
 * Middleware that intercepts all (wanted or unexpected) errors from the application
 * and format the response
 * @param {*} ctx
 * @param {*} next
 */
async function errorHandler(ctx, next) {
  try {
    await next();
  } catch (err) {
    ctx.status = err.statusCode || 500;
    if (ctx.status === 500) {
      ctx.body = {
        code: 'INTERNAL_SERVER_ERROR',
        message: 'An internal error occured. Sorry :(',
      };
    } else {
      ctx.body = err.toJSON ? err.toJSON() : { message: err.message, ...err };
    }
  }
}

module.exports = errorHandler;

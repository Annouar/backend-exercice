class ApplicationError extends Error {
  constructor(status, code, message) {
    super(message);
    this.statusCode = status;
    this.code = code;
    this.message = message;
  }

  toJSON() {
    return {
      statusCode: this.statusCode,
      code: this.code,
      message: this.message,
    };
  }
}

class NotFoundError extends ApplicationError {
  constructor(code, message) {
    super(404, code, message);
  }
}

class PlayerNotFound extends NotFoundError {
  constructor(message) {
    super('PLAYER_NOT_FOUND', message);
  }
}

class BadRequestError extends ApplicationError {
  constructor(code, message) {
    super(400, code, message);
  }
}

class PlayerIdBadFormat extends BadRequestError {
  constructor(message) {
    super('PLAYER_ID_BAD_FORMAT', message);
  }
}

class ServiceUnavailableError extends ApplicationError {
  constructor(code, message) {
    super(503, code, message);
  }
}

class PlayersServiceError extends ServiceUnavailableError {
  constructor(message) {
    super('PLAYERS_SERVICE_ERROR', message);
  }
}

module.exports = {
  ApplicationError,
  PlayerNotFound,
  PlayerIdBadFormat,
  PlayersServiceError,
};

const Koa = require('koa');
const errorHandler = require('./middlewares/error-handler');
const routes = require('./routes');

const app = new Koa();

app.use(errorHandler);
app.use(routes.routes());
app.use(routes.allowedMethods());

app.on('error', (err) => {
  console.error('server error', err);
});

module.exports = app;

const Router = require('koa-router');
const healthcheckRoutes = require('./healthcheck');
const playersRoutes = require('./players');

const router = new Router();

router
  .use('/healthcheck', healthcheckRoutes.routes(), healthcheckRoutes.allowedMethods())
  .use('/players', playersRoutes.routes(), playersRoutes.allowedMethods());

module.exports = router;

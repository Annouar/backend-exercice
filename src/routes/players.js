const Router = require('koa-router');
const { getPlayersData, checkPlayerIdParam } = require('../middlewares/players');
const { PlayerNotFound } = require('../errors');

const router = new Router();

router
  .use(getPlayersData)
  .param('playerId', checkPlayerIdParam)
  .get('/', async (ctx, next) => {
    const orderedPlayers = ctx.state.players.sort((a, b) => {
      if (a.id > b.id) {
        return 1;
      }
      return -1;
    });

    ctx.body = orderedPlayers;

    next();
  })
  .get('/:playerId', async (ctx, next) => {
    const wantedPlayer = ctx.state.players.find(
      player => player.id === ctx.state.sanitizedPlayerId,
    );

    if (!wantedPlayer) {
      throw new PlayerNotFound(`Player #${ctx.state.sanitizedPlayerId} not found`);
    }

    ctx.body = wantedPlayer;

    next();
  });

module.exports = router;

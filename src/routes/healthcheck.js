const Router = require('koa-router');

const router = new Router();

router.get('/', async (ctx) => {
  ctx.body = 'App is running :)';
});

module.exports = router;

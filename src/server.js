const http = require('http');
const config = require('config');
const app = require('./app');

const server = http.createServer(app.callback());

server.listen(config.port, config.host, () => {
  console.log(`Koa server running on port ${config.port}.`);
});

# Back-End Exercise

A solution to backend exercice

## Run the project

```shell
# With npm
$ npm install
$ npm start

# With yarn
$ yarn install
$ yarn start
```

## Test project

```shell
# With npm
$ npm run test

# With yarn
$ yarn test
```
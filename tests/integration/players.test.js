const request = require('supertest');
const config = require('config');
const app = require('../../src/app');
const playersApp = require('../mock/fake-server/server');

describe('Players routes', () => {
  let server;

  beforeAll(() => {
    server = playersApp.listen(config.get('fakeServerPort'));
  });

  afterAll(() => {
    server.close();
  });

  describe('GET /players', () => {
    it('should return a list of players sorted by id', async () => {
      const response = await request(app.callback()).get('/players');

      expect(response.status).toBe(200);
      expect(Array.isArray(response.body)).toBe(true);
      const players = response.body;
      expect(players.map(player => player.id)).toEqual([1, 2, 3, 4, 5]);
    });
  });

  describe('GET /players/:playerId', () => {
    it('should return the player with specific id', async () => {
      const playerId = 1;
      const response = await request(app.callback()).get(`/players/${playerId}`);
      expect(response.status).toBe(200);
      const player = response.body;
      expect(player.id).toBe(playerId);
    });

    it('should return a 404 if player not found', async () => {
      const playerId = 999999;
      const response = await request(app.callback()).get(`/players/${playerId}`);

      expect(response.status).toBe(404);
      expect(response.body.statusCode).toBe(404);
      expect(response.body.code).toBe('PLAYER_NOT_FOUND');
      expect(response.body.message).toBe(`Player #${playerId} not found`);
    });

    it('should return a 400 if player id has an invalid format', async () => {
      const response = await request(app.callback()).get('/players/rr');

      expect(response.status).toBe(400);
      expect(response.body.statusCode).toBe(400);
      expect(response.body.code).toBe('PLAYER_ID_BAD_FORMAT');
      expect(response.body.message).toBe('Player Id format is not correct. It should be an Integer. Found: rr');
    });
  });
});

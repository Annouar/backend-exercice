const Koa = require('koa');
const Router = require('koa-router');
const data = require('./db.json');

const app = new Koa();
const router = new Router();

router.get('/players', (ctx, next) => {
  ctx.body = data;
  next();
});

app.use(router.routes());
app.use(router.allowedMethods());

app.on('error', (err) => {
  console.error('server error', err);
});

module.exports = app;

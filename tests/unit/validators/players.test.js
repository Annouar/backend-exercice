const { playersValidator } = require('../../../src/validators');

describe('Validators > players', () => {
  describe('isPlayerId', () => {
    it('should  validate a player id', () => {
      const goodFormatPlayersIds = ['0', '12', '199'];
      const badFormatPlayersIds = ['-1', 'abc', '  23  '];

      goodFormatPlayersIds.forEach(playerId => expect(playersValidator.isPlayerId(playerId)).toBe(true));
      badFormatPlayersIds.forEach(playerId => expect(playersValidator.isPlayerId(playerId)).toBe(false));
    });
  });
});

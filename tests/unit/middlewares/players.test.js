const mockAxios = require('axios');
const { getPlayersData, checkPlayerIdParam } = require('../../../src/middlewares/players');
const { playersValidator } = require('../../../src/validators');

jest.mock('axios');
jest.mock('../../../src/validators');

describe('Middlewares > players', () => {
  describe('getPlayersData', () => {
    it('should fetch data and put into ctx.state.players', async () => {
      // Given
      const ctx = { state: {} };
      const next = jest.fn();
      const players = [{ id: 1, name: 'foo' }, { id: 2, name: 'bar' }];
      mockAxios.get.mockImplementationOnce(() => Promise.resolve({ data: { players } }));
      // When
      await getPlayersData(ctx, next);
      // Then
      expect(mockAxios.get).toHaveBeenCalledTimes(1);
      expect(ctx.state.players).toEqual(players);
      expect(next).toHaveBeenCalledTimes(1);
    });

    it('should throw a PlayersServiceError if an error occurs with fetch request', async (done) => {
      // Given
      mockAxios.get.mockImplementationOnce(() => Promise.reject());
      const next = jest.fn();
      // When
      try {
        await getPlayersData({}, next);
        done.fail();
      } catch (err) {
        expect(err.statusCode).toBe(503);
        expect(err.message).toBe('Players service not reachable');
        expect(err.code).toBe('PLAYERS_SERVICE_ERROR');
        done();
      }
    });

    it('should throw PlayersServiceError if the response data does not contains an array of players', async (done) => {
      // Given
      const next = jest.fn();
      mockAxios.get.mockImplementationOnce(() => Promise.resolve({ data: {} }));
      // When
      try {
        await getPlayersData({}, next);
        done.fail();
      } catch (err) {
        expect(err.statusCode).toBe(503);
        expect(err.message).toBe('Players service returns a bad format for players');
        expect(err.code).toBe('PLAYERS_SERVICE_ERROR');
        done();
      }
    });
  });

  describe('checkPlayerIdParam', () => {
    it('should check if param id is ok, sanitize it and put it into ctx.state.sanitizedPlayerId', async () => {
      // Given
      const ctx = { state: {} };
      const next = jest.fn();
      playersValidator.isPlayerId.mockReturnValueOnce(true);
      // When
      await checkPlayerIdParam('12', ctx, next);
      // Then
      expect(ctx.state.sanitizedPlayerId).toBe(12);
      expect(next).toHaveBeenCalledTimes(1);
    });

    it('should throw PlayerIdBadFormat if playerId is not valid', async (done) => {
      // Given
      playersValidator.isPlayerId.mockReturnValueOnce(false);
      const next = jest.fn();
      // When
      try {
        await checkPlayerIdParam('rr', {}, next);
        done.fail();
      } catch (err) {
        expect(err.statusCode).toBe(400);
        expect(err.message).toBe('Player Id format is not correct. It should be an Integer. Found: rr');
        expect(err.code).toBe('PLAYER_ID_BAD_FORMAT');
        done();
      }
    });
  });
});

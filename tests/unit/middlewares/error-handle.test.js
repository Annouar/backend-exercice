const errorHandler = require('../../../src/middlewares/error-handler');

describe('Middlewares > errorHandler', () => {
  it('should call next middleware', async () => {
    // Given
    const next = jest.fn();
    // When
    await errorHandler({}, next);
    // Then
    expect(next).toHaveBeenCalledTimes(1);
  });

  it('should create an interval server error if an unexpected error occurs', async () => {
    // Given
    const ctx = {};
    const next = jest.fn().mockImplementation(() => {
      throw new Error();
    });
    // When
    await errorHandler(ctx, next);
    // Then
    expect(ctx.status).toBe(500);
    expect(ctx.body).toEqual({
      code: 'INTERNAL_SERVER_ERROR',
      message: 'An internal error occured. Sorry :(',
    });
  });
});
